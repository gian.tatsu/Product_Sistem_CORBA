
import java.sql.*;

public class ConnectionSQLITE {
    public static Connection get( ){
      Connection c = null;
      try {
        Class.forName("org.sqlite.JDBC");
        System.out.println("Luego get class bye name");
        c = DriverManager.getConnection("jdbc:sqlite:ProductBD.sqlite");
        System.out.println("Opened database successfully");
        return c;
      } catch ( Exception e ) {
        System.out.println("Entra al catch");
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        System.exit(0);
      }
      return null;
    }
}