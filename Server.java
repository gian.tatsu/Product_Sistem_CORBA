import ProductApp.Product;
import ProductApp.ProductHelper;
import java.io.IOException;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.CORBA.Object;

/**
 *
 * @author José Santos
 */
public class Server {
    public static void main(String []args) {
        try {
            // Here we initiate the server with the orbd
            // For default we specify Port 2000 and Host localhost
            /*try {
                Runtime.getRuntime().exec("orbd -ORBInitialPort 36935 InitialHost 127.0.1.1");
            }
            catch(IOException exc) {
                exc.printStackTrace();
            }*/
           
            ORB orb = ORB.init(args, null);
            POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();
            
            ImplementProduct implementProduct = new ImplementProduct();
            implementProduct.setOrb(orb);
            
            org.omg.CORBA.Object ref = rootpoa.servant_to_reference(implementProduct);
            Product href = ProductHelper.narrow(ref);
            
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            
            NameComponent path[] = ncRef.to_name("Product");
            ncRef.rebind(path, href);
            
            System.out.println("Servidor Iniciado");
            orb.run();
        }
        catch(Exception exc) {
            exc.printStackTrace();
        }
    }
}